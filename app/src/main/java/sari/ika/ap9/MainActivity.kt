package sari.ika.ap9

import android.media.MediaPlayer
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.view.View
import android.widget.MediaController
import android.widget.SeekBar
import kotlinx.android.synthetic.main.activity_main.*
import java.util.concurrent.TimeUnit

class MainActivity : AppCompatActivity(), View.OnClickListener, SeekBar.OnSeekBarChangeListener {

    override fun onStartTrackingTouch(seekBar: SeekBar?) {

    }

    override fun onStopTrackingTouch(seekBar: SeekBar?) {
        seekBar?.progress?.let { mediaPlayer.seekTo(it) }
    }

    override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {

    }

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.btnPlay ->{
                audioPlay(poslaguskrg)
            }
            R.id.btnNext ->{
                audioNext()
            }
            R.id.btnPrev ->{
                audioPrev()
            }
            R.id.btnStop ->{
                audioStop()
            }
        }
    }

    val daftarlagu = intArrayOf(R.raw.music_1, R.raw.music_2, R.raw.music_3)
    val daftarvideo = intArrayOf(R.raw.vid_1, R.raw.vid_2, R.raw.vid_3)
    val daftarcover = intArrayOf(R.drawable.cover_1, R.drawable.cover_2, R.drawable.cover_3)

    var poslaguskrg = 0
    var posvidskrg = 0
    var handler = Handler()
    lateinit var mediaPlayer: MediaPlayer
    lateinit var mediaController: MediaController


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        mediaController = MediaController(this)
        mediaPlayer = MediaPlayer.create(this, daftarlagu[poslaguskrg])
        imV.setImageResource(daftarcover[poslaguskrg])
        seekSong.max=100
        seekSong.progress = 0
        seekSong.setOnSeekBarChangeListener(this)
        btnNext.setOnClickListener(this)
        btnPlay.setOnClickListener(this)
        btnPrev.setOnClickListener(this)
        btnStop.setOnClickListener(this)
        mediaController.setPrevNextListeners(nextVid,prevVid)
        mediaController.setAnchorView(videoView2)
        videoView2.setMediaController(mediaController)
        videoSet(posvidskrg)
    }

    fun videoSet(pos : Int){
        videoView2.setVideoURI(Uri.parse("android.resource://"+packageName+"/"+daftarvideo[pos]))
    }

    var nextVid = View.OnClickListener {
        if(posvidskrg<(daftarvideo.size-1)) posvidskrg++
        else posvidskrg = 0
        videoSet(posvidskrg)
    }

    var prevVid = View.OnClickListener {
        if(posvidskrg>0) posvidskrg--
        else posvidskrg = daftarvideo.size-1
        videoSet(posvidskrg)
    }

    fun milliSecondToString(ms : Int):String{
        var detik:Long = TimeUnit.MILLISECONDS.toSeconds(ms.toLong())
        val menit:Long = TimeUnit.SECONDS.toMinutes(detik)
        detik = detik % 60
        return "$menit : $detik"
    }

    fun audioPlay(pos : Int){
        mediaPlayer = MediaPlayer.create(this, daftarlagu[pos])
        seekSong.max = mediaPlayer.duration
        txMaxTime.setText(milliSecondToString(seekSong.max))
        txCrTime.setText(milliSecondToString(mediaPlayer.currentPosition))
        seekSong.progress = mediaPlayer.currentPosition
        imV.setImageResource(daftarcover[pos])
        txJudulLagu.setText(daftarlagu[pos])
        mediaPlayer.start()
        var updateSeekBarThread = UpdateSeekBarProgressThread()
        handler.postDelayed(updateSeekBarThread, 50)
    }

    fun audioNext(){
        if(mediaPlayer.isPlaying)mediaPlayer.stop()
        if(poslaguskrg<daftarlagu.size-1) {
            poslaguskrg++
        } else {
            poslaguskrg = 0
        }
        audioPlay(poslaguskrg)
    }

    fun audioPrev(){
        if(mediaPlayer.isPlaying)mediaPlayer.stop()
        if(poslaguskrg>0) {
            poslaguskrg--
        } else {
            poslaguskrg = daftarlagu.size-1
        }
        audioPlay(poslaguskrg)
    }

    fun audioStop(){
        if(mediaPlayer.isPlaying)mediaPlayer.stop()
    }

    inner class UpdateSeekBarProgressThread : Runnable{
        override fun run() {
            var currTime = mediaPlayer.currentPosition
            txCrTime.setText(milliSecondToString(currTime))
            seekSong.progress=currTime
            if(currTime != mediaPlayer.duration) handler.postDelayed(this, 50)
        }
    }
}

